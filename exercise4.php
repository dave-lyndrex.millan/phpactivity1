<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.js" integrity="sha512-n/4gHW3atM3QqRcbCn6ewmpxcLAHGaDjpEBu4xZd47N0W2oQ+6q7oc3PXstrJYXcbNU1OHdQ1T7pAP+gi5Yu8g==" crossorigin="anonymous"></script>
    <title>PHP Activity</title>
</head>
<body>

<center>
<?php
include_once('index.php');
?>
 <h3>Exercise Number 4</h3><br>
 <h4>Create a function to test the loyalty of a name based on the number of letters in the name and the occurences of the letters E, A, or N in the string.
</h4>
<br>
<div class="container">
    <form action="" method="POST">
        Input Name: <input type="text" name="name">
        <input type="submit" name="submit" class="btn btn-outline-primary btn-sm">
    </form>
</div>

<?php
if(isset($_POST['submit'])){
    $name = $_POST['name'];

    function testLoyalty($name){
        $numOfE = substr_count($name, 'E');
        $numOfA = substr_count($name, 'A');
        $numOfN = substr_count($name, 'N'); 

        $length = strlen($name);
       
        if (($numOfE + $numOfA + $numOfN) > 3){
            if ((($numOfE + $numOfA + $numOfN)* $length) % 6 == 0 ){
                echo "testLoyalty('$name')<- Loyal" ;
            }else {
                echo "testLoyalty('$name')<- Di sure";
            } 
        }else {
            echo "testLoyalty('$name')<- Di sure";
        }
    }testLoyalty($name);
}


?>
 </center>
</body>
</html>