<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.js" integrity="sha512-n/4gHW3atM3QqRcbCn6ewmpxcLAHGaDjpEBu4xZd47N0W2oQ+6q7oc3PXstrJYXcbNU1OHdQ1T7pAP+gi5Yu8g==" crossorigin="anonymous"></script>
    <title>Exercise Number 1</title>
</head>
<body>

<center>
<?php
include_once('index.php');
?>

<h2>Exercise Number 1</h2>  <br>

  <?php

include_once('index.php');
echo ('<center>'.'<br>');
echo('<h3>'."Create a function that will show the nth line of a text file if it exists." .'</h3'.'<br>'.'<br>'.'<br>');
function checkFile( $file, $keyword ) {
    
    // open file for reading
    $handle = @fopen( $file, 'r' );
   
    if( $handle ) {
       while( ($line = fgets($handle)) !== false ) {
            // search for specific keyword no matter what case is used i.e. dave or Dave          
            if( stripos($line, $keyword) === false ) {            
                continue;              
            } else {            
                fclose($handle);       
                return $line;               
            }
        }
    }
}
//change your text here.
$result = checkFile( 'ex1.txt', '2' );

echo $result;

?>

</center>
</body>
</html>


