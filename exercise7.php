<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.js" integrity="sha512-n/4gHW3atM3QqRcbCn6ewmpxcLAHGaDjpEBu4xZd47N0W2oQ+6q7oc3PXstrJYXcbNU1OHdQ1T7pAP+gi5Yu8g==" crossorigin="anonymous"></script>
    <title>PHP Activity</title>
</head>
<body>
<center>
  <?php  
  include_once('index.php');
  ?>
 <h3>Exercise Number 7</h3>  <br><br>
<h4>Write a function to reverse a string. If the string has an even number for its length, grab only half of the reversed string.</h4>
<br>
<div class="container">
    <form action="" method="POST">
        Input word/words: <input type="text" name="userInput" >
        <button type="btn" class="btn btn-outline-primary btn-sm" name="reverse">Submit</button>
        <br>
       <strong> output: </strong>
    </form>
</div>

<?php
if(isset($_POST['reverse'])){
    $string = $_POST['userInput'];

    function reverse($userInput){
        $revString = strrev($userInput);
        $len = strlen($revString);

        $half = $len/-2;

        if($len % 2 != 0){
           return $revString;
        }else {
            $pass = substr($revString, 0, $half);
            return $pass;
        }
    }
    $userInput = $string;
    print_r(reverse($userInput));
}

?>

</center>
</body>
</html>