<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.js" integrity="sha512-n/4gHW3atM3QqRcbCn6ewmpxcLAHGaDjpEBu4xZd47N0W2oQ+6q7oc3PXstrJYXcbNU1OHdQ1T7pAP+gi5Yu8g==" crossorigin="anonymous"></script>
    <title>PHP Activity</title>
</head>
<body>

<center>
<?php
include_once('index.php');
?>
  <h3>Exercise Number 5</h3> <br>
  <h4>Write a function to calculate the factorial of a number. 
  The function accepts the number as an argument. If the argument given is a negative number, get its absolute value.</h4>
<br>
<div class="container">
    <form action="" method="POST">
        Input any number: <input type="text" name="userInput">
        <input type="submit" class="btn btn-outline-primary btn-sm" name="factorial">
    </form>
</div>

<?php
if(isset($_POST['factorial'])){
    $userInput = $_POST['userInput'];

    function factorial($num){
        if ($num < 0){
            $num = (abs($num));
        }else{
           $num = $num;
        }
        $factorial = 1;
        for ($i= $num; $i >= 1; $i--){
            $factorial = $factorial * $i;
        }
        echo "The factorial of " . $num . " is " . $factorial;
    }
    factorial($userInput);
}
?>
</center>
</body>
</html>