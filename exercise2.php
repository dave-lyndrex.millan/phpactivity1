<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.js" integrity="sha512-n/4gHW3atM3QqRcbCn6ewmpxcLAHGaDjpEBu4xZd47N0W2oQ+6q7oc3PXstrJYXcbNU1OHdQ1T7pAP+gi5Yu8g==" crossorigin="anonymous"></script>
    <title>Exercise Number 2</title>
</head>
<body>

<center>
<?php
include_once('index.php');
?>
  <h3>Exercise Number 2</h3><br>
  <h4>Create a function that allows you to append a line into a file given the line number</h4>

<?php
function exFile($fileName, $data, $lineNum){
  if(file_exists($fileName)){
    $file = fopen($fileName, 'a');
    $lines = file ($fileName, FILE_IGNORE_NEW_LINES);
    array_splice($lines, $lineNum-1, 0, $data);
    file_put_contents($fileName, join("\n", ($lines)));
    fclose($file);

  }
}
exFile('ExerciseThree.txt', '#teamBangan',3);

echo "Successfully added!";
?>
  </center>
</html>